create_company_201 = {
    'company': {
        'id': 'bf88a0ce-b5c3-4912-b60b-051d57091cbb',
        'name': 'Mi compañía',
        'tradeName': 'Mi compañía S.A',
        'identification': '12345678',
        'address': '30 De Marzo 9 Sd',
        'province': 'Santo domingo',
        'municipality': '30 De Marzo 9 Sd',
        'type': 'associated',
        'webhooks': {}
    }
}
