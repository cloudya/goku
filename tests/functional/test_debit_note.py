# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.debit_note_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E330000000005",
                "sequenceDueDate": "2010-10-10",
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 1000124545,
                "companyName": "Mi compañía",
                "address": "30 De Marzo 9 Sd",
                "stampDate": "2022-10-10"
            },
            "buyer": {
                "rnc": 1000124545,
                "companyName": "Su compañía"
            },
            "totals": {
                "totalAmount": 88757.68
            },
            "itemDetails": [{
                "retention": {},
                "otherCurrencyDetail": {},
                "lineNumber": 1,
                "billingIndicator": 1,
                "itemName": "Caja de madera",
                "goodServiceIndicator": 1,
                "quantityItem": 24300,
                "unitPriceItem": 24300,
                "itemAmount": 24300
            }],
            "informationReference": {
                "ncfModified": '1090012500',
                "modificationCode": 1
            },
        }

    def test_generate_debit_note(self):
        # Act
        response = self.goku.generate_debit_note(self.debit_note_data)

        # Assert
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    def test_generate_debit_note_for_a_client(self):
        # Arrange
        self.debit_note_data['company'].update(
            {'id': self.client_of_cy_company_id})

        # Act
        response = self.goku.generate_debit_note(self.debit_note_data)

        # Assert
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    def test_get_debit_note_detail(self):
        # Arrange
        debit_note_id = self.test_generate_debit_note()

        # Act
        response = self.goku.get_debit_note(debit_note_id=debit_note_id)

        # Assert
        self.assertEqual(response.status_code, 200)

    def test_get_debit_note_detail_for_a_client(self):
        # Arrange
        debit_note_id = self.test_generate_debit_note_for_a_client()

        # Act
        response = self.goku.get_debit_note_by_company(
            debit_note_id=debit_note_id,
            company_id=self.client_of_cy_company_id)

        # Assert
        self.assertEqual(response.status_code, 200)
