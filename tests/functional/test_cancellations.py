# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku
import time


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.cancelation_data = {
            "header": {
                "cancelledEncfQuantity": 1,
                "rncSender": 132109122
            },
            "cancellations": [
                {
                    "rangeCancelledEnfc": [
                        {
                            "encfFrom": "E310000000009",
                            "encfUntil": "E310000000009"
                        }
                    ],
                    "lineNumber": 1,
                    "ecfType": 31,
                    "cancelledEncfQuantity": 1
                }
            ]
        }

    def generate_cancellation(self):
        # Act
        response = self.goku.generate_cancellation(self.cancelation_data)

        # Arrange
        self.assertEqual(response.status_code, 200)
        return response.json()['id']

    def no_test_get_cancellation(self):
        # Arrange
        cancellation_id = self.test_generate_cancellation()

        # Must wait a bit, to avoid a false 404.
        time.sleep(5)

        # Act
        response = self.goku.get_cancellation(
            cancellation_id=cancellation_id)

        # Assert
        self.assertEqual(response.status_code, 200)

    def no_test_get_cancellation_by_company(self):
        # Arrange
        cancellation_id = self.generate_cancellation()

        # Must wait a bit, to avoid a false 404.
        time.sleep(5)

        # Act
        response = self.goku.get_cancellation_by_company(
            cancellation_id=cancellation_id,
            company_id=self.cy_company_id,
        )

        # Assert
        self.assertEqual(response.status_code, 200)
