# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.minor_expense_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E430000000010",
                "sequenceDueDate": "2022-12-31",
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "Mi compañía",
                "address": "30 De Marzo 9 Sd",
                "stampDate": "2022-10-10"
            },
            "buyer": {
                "rnc": 10001245451,
                "companyName": "Su compañía"
            },
            "totals": {
                "exemptAmount": 1000.00,
                "totalAmount": 1000.00
            },
            "itemDetails": [{
                "lineNumber": 1,
                "billingIndicator": 4,
                "itemName": "Caja de madera",
                "goodServiceIndicator": 1,
                "quantityItem": 1,
                "unitPriceItem": 1000.00,
                "itemAmount": 1000.00
            }]
        }

    def test_generate_minor_expense(self):
        # Act
        response = self.goku.generate_minor_expense(self.minor_expense_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_minor_expense_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())
