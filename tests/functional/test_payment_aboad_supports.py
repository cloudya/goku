# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.payment_abroad_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E470000000005",
                "sequenceDueDate": "2010-10-10",
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 1000124545,
                "companyName": "Mi compañía",
                "address": "30 De Marzo 9 Sd",
                "stampDate": "2022-10-10"
            },
            "buyer": {
                "rnc": 1000124545,
                "companyName": "Su compañía"
            },
            "totals": {
                "totalAmount": 88757.68
            },
            "itemDetails": [{
                "retention": {
                    "indicatorAgentWithholdingPerception": 1,
                    "isrAmountWithheld": 500.00
                },
                "otherCurrencyDetail": {},
                "lineNumber": 1,
                "billingIndicator": 1,
                "itemName": "Caja de madera",
                "goodServiceIndicator": 1,
                "quantityItem": 24300,
                "unitPriceItem": 24300,
                "itemAmount": 24300
            }]
        }

    def test_generate_payment_abroad(self):
        # Act
        response = self.goku.generate_payment_abroad(self.payment_abroad_data)

        # Assert
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    def test_generate_payment_abroad_for_a_client(self):
        # Arrange
        self.payment_abroad_data['company'].update(
            {'id': self.client_of_cy_company_id})

        # Act
        response = self.goku.generate_payment_abroad(self.payment_abroad_data)

        # Assert
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    def test_get_payment_abroad_detail(self):
        # Arrange
        payment_abroad_id = self.test_generate_payment_abroad()

        # Act
        response = self.goku.get_payment_abroad(payment_abroad_id=payment_abroad_id)

        # Assert
        self.assertEqual(response.status_code, 200)

    def test_get_payment_abroad_detail_for_a_client(self):
        # Arrange
        payment_abroad_id = self.test_generate_payment_abroad_for_a_client()

        # Act
        response = self.goku.get_payment_abroad_by_company(
            payment_abroad_id=payment_abroad_id,
            company_id=self.client_of_cy_company_id)

        # Assert
        self.assertEqual(response.status_code, 200)
