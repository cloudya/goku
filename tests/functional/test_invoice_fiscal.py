# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku
import pytest


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.encf = 'E310000000062'

        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E310000006070",
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 1,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 1000.00,
                "itbisTotal": 0,
                "i3AmountTaxed": 1000.00,
                "itbisS3": 0,
                "totalAmount": 1000.00,
                "itbis3Total": 0.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 3,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                }
            ]
        }

    def test_generate_invoice_fiscal(self):
        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    @pytest.mark.polo
    def test_generate_invoice_fiscal_for_a_client(self):
        # Arrange
        self.invoice_fiscal_data['company'].update(
            {'id': self.client_of_cy_company_id})

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())
        return response.json()['id']

    def test_get_fiscal_detail(self):
        # Arrange
        invoice_id = self.test_generate_invoice_fiscal()

        # Act
        response = self.goku.get_invoice_fiscal(invoice_id=invoice_id)

        # Assert
        self.assertEqual(response.status_code, 200)

    def test_get_fiscal_detail_for_a_client(self):
        # Arrange
        invoice_id = self.test_generate_invoice_fiscal_for_a_client()

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            invoice_id=invoice_id,
            company_id=self.client_of_cy_company_id)

        # Assert
        self.assertEqual(response.status_code, 200)

    @pytest.mark.s1
    def test_create_e32_s1(self):
        # Arrange
        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E310000006083",
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 0,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 1000.00,
                "i1AmountTaxed": 1000.00,
                "itbisS1": 18,
                "itbisTotal": 180.00,
                "itbis1Total": 180.00,
                "totalAmount": 1180.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 1,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                }
            ]
        }

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())

    @pytest.mark.s1disc
    def test_create_e32_s1disc(self):
        # Arrange
        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": self.encf,
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 0,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 1000.00,
                "i1AmountTaxed": 1000.00,
                "i2AmountTaxed": 0.00,
                "i3AmountTaxed": 0.00,
                "itbisS1": 18,
                "itbisS2": 16,
                "itbisS3": 0,
                "itbisTotal": 180.00,
                "itbis1Total": 180.00,
                "itbis2Total": 0.00,
                "itbis3Total": 0.00,
                "totalAmount": 1180.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 1,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1100.00,
                    "discountAmount": 100.00,
                    "subDiscounts": [
                        {
                            "subDiscountRate": "$",
                            "subDiscountAmount": 100
                        }
                    ],
                    "itemAmount": 1000.00
                }
            ]
        }

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())

    @pytest.mark.s3
    def test_create_e32_s2(self):
        pass

    @pytest.mark.s3
    def test_create_e32_s3(self):
        # Arrange
        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": self.encf,
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 0,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 1000.00,
                "i1AmountTaxed": 0.00,
                "i2AmountTaxed": 0.00,
                "i3AmountTaxed": 1000.00,
                "itbisS1": 18,
                "itbisS2": 16,
                "itbisS3": 0,
                "itbisTotal": 0.00,
                "itbis1Total": 0.00,
                "itbis2Total": 0.00,
                "itbis3Total": 0.00,
                "totalAmount": 1000.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 3,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                },
            ]
        }

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())

    @pytest.mark.s1s2
    def test_create_e32_s1_s2(self):
        # Arrange
        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E310000006085",
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 0,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 2000.00,
                "i1AmountTaxed": 1000.00,
                "i2AmountTaxed": 1000.00,
                "itbisS1": 18,
                "itbisS2": 16,
                "itbisTotal": 340.00,
                "itbis1Total": 180.00,
                "itbis2Total": 160.00,
                "totalAmount": 2340.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 1,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                },
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 2,
                    "billingIndicator": 2,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                }
            ]
        }

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())

    @pytest.mark.s1s2s3
    def test_create_e32_s1_s2_s3(self):
        # Arrange
        self.invoice_fiscal_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": self.encf,
                "sequenceDueDate": "2022-12-31",
                "taxAmountIndicator": 0,
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "TJ Consulting SRL",
                "address": "Carretera Mella No.1",
                "stampDate": "2022-03-11"
            },
            "buyer": {
                "rnc": 130952027,
                "companyName": "Paul Hoffman"
            },
            "totals": {
                "totalTaxedAmount": 3000.00,
                "i1AmountTaxed": 1000.00,
                "i2AmountTaxed": 1000.00,
                "i3AmountTaxed": 1000.00,
                "itbisS1": 18,
                "itbisS2": 16,
                "itbisS3": 0,
                "itbisTotal": 340.00,
                "itbis1Total": 180.00,
                "itbis2Total": 160.00,
                "itbis3Total": 0.00,
                "totalAmount": 3340.00,
            },
            "itemDetails": [
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 1,
                    "billingIndicator": 1,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                },
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 2,
                    "billingIndicator": 2,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                },
                {
                    "retention": {},
                    "otherCurrencyDetail": {},
                    "lineNumber": 3,
                    "billingIndicator": 3,
                    "itemName": "Tomatos",
                    "goodServiceIndicator": 1,
                    "quantityItem": 1,
                    "unitPriceItem": 1000.00,
                    "itemAmount": 1000.00
                }
            ]
        }

        # Act
        response = self.goku.generate_invoice_fiscal(self.invoice_fiscal_data)

        # Assert
        self.assertEqual(response.status_code, 201)

        # Arrange
        document_id = response.json()['id']

        # Act
        response = self.goku.get_invoice_fiscal_by_company(
            document_id, self.client_of_cy_company_id)
        print(response.json())
