# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.functional.base import GokuTestBase
from goku import Goku


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.invoice_data = {
            'company': {
                'id': self.cy_company_id
            },
            "idDoc": {
                "encf": "E320000000005",
                "sequenceDueDate": "2010-10-10",
                "incomeType": 1,
                "paymentType": 1
            },
            "sender": {
                "rnc": 132109122,
                "companyName": "Mi compañía",
                "address": "30 De Marzo 9 Sd",
                "stampDate": "2022-10-10"
            },
            "buyer": {
                "rnc": 10001245451,
                "companyName": "Su compañía"
            },
            "totals": {
                "totalAmount": 24300.10
            },
            "itemDetails": [{
                "retention": {},
                "otherCurrencyDetail": {},
                "lineNumber": 1,
                "billingIndicator": 1,
                "itemName": "Caja de madera",
                "goodServiceIndicator": 1,
                "quantityItem": 24300.10,
                "unitPriceItem": 24300.10,
                "itemAmount": 24300.10
            }]
        }

    def test_generate_invoice(self):
        # Act
        response = self.goku.generate_invoice(self.invoice_data)

        # Arrange
        self.assertEqual(response.status_code, 201)
        import ipdb; ipdb.set_trace()
        return response.json()['id']

    def ptest_generate_invoice_for_a_client(self):
        # Arrange
        self.invoice_data['company'].update(
            {'id': self.client_of_cy_company_id})

        # Act
        response = self.goku.generate_invoice(self.invoice_data)

        import ipdb; ipdb.set_trace()
        self.assertEqual(response.status_code, 201)
        return response.json()['id']

    # def test_get_invoice(self):
    #     # Arrange
    #     invoice_id = self.test_generate_invoice()

    #     # Act
    #     response = self.goku.get_invoice(invoice_id=invoice_id)

    #     # Assert
    #     self.assertEqual(response.status_code, 200)

    # def test_get_for_a_client(self):
    #     # Arrange
    #     invoice_id = self.test_generate_invoice_for_a_client()

    #     # Act
    #     response = self.goku.get_invoice_by_company(
    #         invoice_id=invoice_id,
    #         company_id=self.client_of_cy_company_id)

    #     # Assert
    #     self.assertEqual(response.status_code, 404)
