# -*- coding: utf-8 -*-
from unittest import TestCase
from tests.base import GokuTestBase
from goku import Goku
from unittest import mock
from requests.exceptions import ConnectionError
from goku import exceptions


class GokuTest(GokuTestBase, TestCase):

    def setUp(self):
        super().setUp()
        self.goku = Goku()

        self.company_data = {
            'certificate': {
                'name': 'certificado digital',
                'extension': 'pfx',
                'password': 'clave123',
                'content': 'MIINEQIBAzCCDNcGCSqGSIb3DQEHAaCCDMgEggzEMIIMwDCCBTcGCSqGSIb3DQEHBqCCBSgwggUkAgEAMIIFHQYJKoZIhvcNAQcBMBwGCiqGSIb3DQEMAQYwDgQIpuwy4BBo1uACAggAgIIE8F9lYBwy7nQVB9tA75hTgq8J+UDN5LxFMRt5/Pa7SnfNhaNudwrSyf1OomZfj6RukCQF27BT5VM9DiwcsGXIR9ZzVgnkzZzb61jMwQTE+ZNdxkIYzL8lVhTt/pclLN0Q9hR69t4pUzlkljUhm6Y+kG6/9bpZPgJaQ/SrEsHq40BUQtp+yZ5v3c1DZeSdB+2j9mJgGp4YAt8g8c6oWiL5UYp4zGsWzrnDPuc2ANiJQduF7+DtixmfiTYrRnnaC/Ooz8bLEPEBiGK90cBF5WFQ6ILq/38Krs6ZyZWdAyT6kJXp8KlVYieSqr5LK4/HZZjmF3Ax6wFm1QqAfPIvd6KNBbTwlUdDsE+6EnXdyMR5hWTtlZ28CGW/Z8gYkDNsCjq08VSjEIh5ityJyWs5BoomjWUfzKPFHrh1AyRJU3Y3QPTdBxSniufdTqJatUqYLJhGj9aO9OKWjCp785v1tmKgD4le4GVEZ465sLsqaKk77uWM/vBFWRBj/JZfTpDBk77Ys/lYkUL+IP6W7tvjpQ6fYZB5jgsNob+U9Qi9iz1uj+No3N2ASHyOTKtY/ZlImsGGA6iO67Iv1QRLAoJGCTpG57m8Lte7p3n2CUSYI3Rmp9hUSz94Ilpbkq0O3l0FJsOzundCWVDAU75LLEN1ErvkWBZQHybPxqloEHvQ0Ka41xiW4lJbTyd3W2uZIJnW3T5UZ21/+toLxJIl5FVSJ+J8EXebPnFa+zbD9o4pNPmDsj1+XOiLM91P1KbHpXx30rtEl7FmnfOuQ+dWad2b4ZhPzzugegURz1OXHvjkUzA8uSOPoi42eEypmPjIr5K46Kzi+tBNbZ/mYKJ1WB6FqUByHvfqHwvGV/7yWvhSw/uJvK4ufyY4dIxbZ4Hkq+QFCzBVWwN1B9WzbVtRhHJZORucdO8KZ4XiWPQC4aXjSPjyThZ4Hukovzg0Yab7crvAYRWV6TSXcE2WkylVORv2JWNRR5MI50jxv7NwnnQTiBrMxKf8hGYKUXsHti6Wy1F/CFmo7WVpyGEoijRsOPdATgkyBSIZEpZA0SYP4wSRBStrQO/933LzRg/wz2r20ZdqyRDasyQ76XqJuwJXecAuWdhNpgxA1Tfnar9ljeSAKZeW8QDgCplPFbw4E8TKZJ+BUpC9vfDnre2SeCDp6QE1UmF7ZrYzV8hwFHKEkrmruwVnZCeeqhHGVa8M3f8aUWr+UJMm0dOqbm3ssYRBGxk49iKJPQPWHks4gwf1JVmMU9MFhWc15Q5ei8loOYn8v9OpF0cxt7ud6/VbiU7Ja2NR46cObVRrFSBQiF6xSf27HP+0zHU8owdmmxAa2zUqCH45vMxSD6Eyi+qgufAYdI8ahlCuZLhCrg6UYdgEwqNwtsMLubrSE+RjWVbz/AGaRzZM/5KbhyxnYQk9AM8FgyYBJ0vbyHw6Cvqhbhu0Gsv7sK7GVhXDQNkqghF6S36FFkeOsNgTJp9b7NheyqmaU9UPkXWa1QgNjdIqAknzB8xZEhuyPPMNkHhfLC2Bahmpxfr3oxeyRsYxNs6b5KNdsu6LhuFLMYuJEKIEyzD11bLijcA00ZtX2aOLQImJbL0iqTo3TLdKTJXYkAQ2bAGThILUzXu5sERnLl+pZrXzRkGJMJjyf4tS/wsoiGXGmI/FWmAY3WjTNmUc1/8On8a/s668T3rK6rUwggeBBgkqhkiG9w0BBwGgggdyBIIHbjCCB2owggdmBgsqhkiG9w0BDAoBAqCCBy4wggcqMBwGCiqGSIb3DQEMAQMwDgQI0kzOlWeEghMCAggABIIHCFaluUgWqTGzzrxszGp82mvm4AUwUpkwp1U3ZtQ8hfkai+vPQiqGz1QyHyk/KnDGnqgzOeCztNKdN3Z5o2+vqqAkEYY3QoNGjv/plNlbgBfTDgisJNrte43tA1q62uelq4lH6qHzfP1+enkU6QKHk9FjIXuQ15X4/8vJBwHVpvz1CCe8CMcs0//0uDKX+iDuSvFcUOK7fNLhD403+VvM8O9zIhLQXWhD/vjD1QGZxUT2fFlFTFX9p9XCcn+8xdryHIql6kwjBNlztKeJOBe7CFjfuX3X/QNn+6a/LhqMYikrQIZTFiGHUI6CtjmtKY5JvyRLcKbmWUHstShIsEdBU7opOMt2yvLIceVJvFXl6R2KTMRL0URz5pJkWuY4GuPWVguCAnAQS2YIx/jjlYmmum6h5L/5oWnphdlamTd3/upSvdcxA9yUk0+7+hK3ady0qPy06byal4zZGSiYPm35EUy256qRAHpJFuRM6T0+zbpFS19N1LBoViRPWF57aQ+Mjx2C439MLdljAr5vhaX6COVU+JWMfQh8t4YmWGl5eP+EeFBBF8bSAUWn1InAywYQ1F+erSW3SULy4dp2maf51ZkPzOgxcbx2yZcHDOGcOMj1/4bO1rNbX56VqOfEl963wHdTnlXH4mybNHWUL+UHz+mAubqCo0oQ78uKT9KRBpKwqBc8mRude4I8ZGCH83ViVVMvdfiHJWARRlKsPaWmttZHURzTlLhyR5cp0Q6e8ug7lHJQioE93PnmVobEXfEEFAwp4x19Y4ImjTmtpdY+4MULMuUt4AzQJEkWXY10yUMKdGURDbd4WW+TOVb+XIBEIjFxPl/TjA1F9a0mdCCrO/wKxlUr94FASHDNH52i7sw9sb2vWhRazAuJ2vLTK86gRF1syCa47lTzYuqw/4bACkQYPX+pripbDkxrVu3NX9B4+ur9ueyGljyGA5GmLbfRexX1VlR2S42wsEDuDaMYDNanoPDFGClQ9TIqFXRxjPIkMarrnxzTKt2tlVmi3H/ZwCJCX68yVerESN63mkCvGaY/Iby0MwfR/MG2jUU6j1aAOQHsQZiWfervQ4CrOq5z/wTo9Vg4roH92rl/oSrSVOuCgaduzTS+6lAU17KyMHUk/3uIJQt/LbKtJxMDtHPpzV3ltNpb1x84/rb7jp9E/ays4QhRS2SZBp4izs3Yj9eUeBoybYjNk+11qUj1fXeBGvLblwzZk2UZ6cl6/4qHA+U8INnmDPyjKQywEy8UB6nrVlh5rxXBjEr/XIKPDRSNDd48cFiRcpSbyUbobRx6gXPzydIV65L6zoiISX1sYWB5v6Hwxo7DxwVi05jDbi09mLOQhdJfkGAgf/d4i7fogwpfLAl2KwylqB4cDuaX3HpKjrPTCn9MXxiAEG5b/YxFi9BlZGO4mYZdWjzahF8nyWUQL9StMeglZbPfmd3Fyn7wZj7Zvk+19fe52ecg0P6Kq4fhQY9i+4ksGZbCcDhdmLQCHNtwi+xT7IWY1z8C/08JQZvg8OgQR3kgQZJOYZ56QYFBJ6M0R6bd7PksY46ia1bhSx5/OeBAwgr4XK5MEH55rzbOc7I6WjiTJDhhJb19sk5HPEJVRfndrt2ub0s8rXt77DQogoekhJ0c+UtE+LFE9EOsmg8rlSC7521KpzQObqGnIV1N5tM3DYdPuwgQFmjLkg9rl/VkSMLm9E8SmcT6P6fNueHVUXSTErvmTiLn1lFkFLlKA1JU5kaI2PU4hXrvhPEr5PeUmvj/WtClDH1vuaQ/0Vp8Pe3CMGOuorHHnYJZcBiq7alvogJvZOymX//L3gagqH/2edXnxcHcESdrWOy+QWaGrow2xZTnpXYAg9ZcQIsJC4O4mmtMOu6Luv6kSb7lyIAGzuIIqCHfROv0Rt7u7k55ZsLpYj2FJFiPjvSy5pNMuI6f60hkhb1E+VjPRv5kOFxJ9DTgdBNFAmyV3eyo5eGAZaIEs/ke+but2uCoPgMlh6ZwMFhXiKrr3P5Q7R1i4QbFGvO+zEyV1ZbZEo2V/Y63eUyVCwsQMbbo7ZISpWHMT1L4PttQ8vc+rD2zWkuZDbKaBBeM1jktKOzbH4V+WK2swc1eaux6fHAKvRR2kxoVAPWnMY5o06U22xI3dYWI7PgWPOT2+a8EyvRpjO3qsoHjHjMAXAz1NTXl9TlnYxi7L4P8brvGwo4Y0zC8o22aLn2lTEdCEt9vyX6FwQsgb3TmSImoKGPZ0HKAQqZ26n7hgp2fdw31aAGwUwrLpY7dqz+PAXf0lppickfIW9T1+F5gOjE0Puf8LV8vB8HdoR/7wO36+yLa2vY9OlhzAjoLVtv03kG5aTpqZdG275bYlz4LEjS5V/jyWJzyqvf1Ev3h2+hU7keudzJeobwZDKENd8kmTjElMCMGCSqGSIb3DQEJFTEWBBSJjcBazPk2DWmZCES2ESu0xWXOaDAxMCEwCQYFKw4DAhoFAAQU3fCG90zB9FMbeijOPnw5KHkdhEQECODhuvCEG+GLAgIIAA=='
            },
            'name': 'Mi compañía',
            'tradeName': 'Mi compañía S.A',
            'identification': '12345678',
            'province': 'Santo domingo',
            'address': '30 De Marzo 9 Sd',
            'municipality': '30 De Marzo 9 Sd'
        }

    @mock.patch('requests.post')
    def test_create_a_company(self, mock_post):
        # Act
        self.goku.create_company(self.company_data)

        # Assert
        self.assertEqual(mock_post.call_args[0][0],
                         'https://sandbox.alanube.co/dom/v1/company')

    @mock.patch('requests.get')
    def test_get_company(self, mock_post):
        # Act
        self.goku.get_company(company_id=self.client_of_cy_company_id)

        # Assert
        self.assertEqual(
            mock_post.call_args[0][0],
            'https://sandbox.alanube.co/dom/v1/company/bf88a0ce-b5c3-4912-b60b-051d57091cbb'
        )

    @mock.patch('requests.post')
    def test_create_company_raises(self, mock_post):
        # Arrange
        mock_post.side_effect = [
            ConnectionError,
        ]

        # Act
        with self.assertRaises(exceptions.ServiceUnavailable):
            self.goku.create_company(self.company_data)

    @mock.patch('requests.get')
    def test_get_company_raises(self, mock_get):
        # Arrange
        mock_get.side_effect = [
            ConnectionError,
        ]

        # Act
        with self.assertRaises(exceptions.ServiceUnavailable):
            self.goku.get_company(company_id=self.client_of_cy_company_id)

    @mock.patch('requests.patch')
    def test_update_company_raises(self, mock_patch):
        # Arrange
        mock_patch.side_effect = [
            ConnectionError,
        ]

        # Act
        with self.assertRaises(exceptions.ServiceUnavailable):
            self.goku.update_company(
                company_id=self.client_of_cy_company_id, data=self.company_data)
