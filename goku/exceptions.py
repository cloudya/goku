

class ServiceUnavailable(Exception):
    status_code = 503
    default_detail = 'Service temporarily unavailable, try again later.'
    default_code = 'service_unavailable'


class Forbidden(Exception):
    status_code = 403
