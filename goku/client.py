# -*- coding: utf-8 -*-
import os
import requests
from goku import exceptions


class BaseAPIClient:
    _headers_base = {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
    }
    _lib = requests


class APIClient(BaseAPIClient):

    def authenticate(self):
        self._headers_base.update(
            {'Authorization': 'Bearer %s' % os.environ.get('GOKU_TOKEN')}
        )

    def get(self, url):
        self.authenticate()

        try:
            response = self._lib.get(
                url,
                headers=self._headers_base,
            )
        except requests.exceptions.ConnectionError as ex:
            raise exceptions.ServiceUnavailable(ex)

        return self.return_value(response)

    def post(self, url, data={}):
        self.authenticate()

        try:
            response = self._lib.post(
                url,
                json=data,
                headers=self._headers_base,
            )
        except requests.exceptions.ConnectionError as ex:
            raise exceptions.ServiceUnavailable(ex)

        return self.return_value(response)

    def patch(self, url, data={}):
        self.authenticate()

        try:
            response = self._lib.patch(
                url,
                json=data,
                headers=self._headers_base,
            )
        except requests.exceptions.ConnectionError as ex:
            raise exceptions.ServiceUnavailable(ex)

        return self.return_value(response)

    def response_to_json(self, response):
        return response.json()

    def return_value(self, response):
        if response.status_code == 200 or response.status_code == 201:
            return response

        if response.status_code == 403:
            raise exceptions.Forbidden(self.response_to_json(response))
        return response
