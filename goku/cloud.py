# -*- coding: utf-8 -*-
import os
from goku.client import APIClient
import time


class Goku:
    _url = os.environ.get('GOKU_API_URL')

    def __init__(self):
        self.api_client = APIClient()

    def get_url(self, resource):
        return '%s%s' % (self._url, resource)

    def get_full_url(self, url):
        return '{base_url}{url}'.format(base_url=self._url, url=url)

    def get_company_info(self):
        return self.api_client.get(self.get_url('company'))

    def create_company(self, data={}):
        return self.api_client.post(self.get_url('company'), data)

    def get_company(self, company_id):
        url = '%s/%s' % (self.get_url('company'), company_id)
        return self.api_client.get(url)

    def update_company(self, company_id, data={}):
        url = '%s/%s' % (self.get_url('company'), company_id)
        return self.api_client.patch(url, data)

    def generate_invoice_fiscal(self, data={}):
        url = 'fiscal-invoices'
        return self.api_client.post(self.get_full_url(url), data)

    def get_invoice_fiscal(self, invoice_id):
        path_url = 'fiscal-invoices/{id}'
        url = path_url.format(id=invoice_id)
        return self.api_client.get(self.get_full_url(url))

    def get_invoice_fiscal_by_company(self, invoice_id, company_id, wait=2):
        path_url = 'fiscal-invoices/{id}/idCompany/{idCompany}'
        url = path_url.format(id=invoice_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, invoice_id, company_id,
                                     'get_invoice_fiscal_by_company', wait)
        return response

    def generate_invoice(self, data={}):
        url = 'invoices'
        return self.api_client.post(self.get_full_url(url), data)

    def get_invoice(self, invoice_id):
        path_url = 'invoices/{id}'
        url = path_url.format(id=invoice_id)
        return self.api_client.get(self.get_full_url(url))

    def get_invoice_by_company(self, invoice_id, company_id, wait=2):
        path_url = 'invoices/{id}/idCompany/{idCompany}'
        url = path_url.format(id=invoice_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, invoice_id, company_id,
                                     'get_invoice_by_company', wait)
        return response

    def generate_special_regimes(self, data={}):
        url = 'special-regimes'
        return self.api_client.post(self.get_full_url(url), data)

    def get_special_regimes(self, invoice_id):
        path_url = 'special-regimes/{id}'
        url = path_url.format(id=invoice_id)
        return self.api_client.get(self.get_full_url(url))

    def get_special_regimes_by_company(self, invoice_id, company_id, wait=2):
        path_url = 'special-regimes/{id}/idCompany/{idCompany}'
        url = path_url.format(id=invoice_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, invoice_id, company_id,
                                     'get_special_regimes_by_company', wait)
        return response

    def generate_gubernamentals(self, data={}):
        url = 'gubernamentals'
        return self.api_client.post(self.get_full_url(url), data)

    def get_gubernamentals(self, invoice_id):
        path_url = 'gubernamentals/{id}'
        url = path_url.format(id=invoice_id)
        return self.api_client.get(self.get_full_url(url))

    def get_gubernamentals_by_company(self, invoice_id, company_id, wait=2):
        path_url = 'gubernamentals/{id}/idCompany/{idCompany}'
        url = path_url.format(id=invoice_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, invoice_id, company_id,
                                     'get_gubernamentals_by_company', wait)
        return response

    def generate_export_support(self, data={}):
        url = 'export-supports'
        return self.api_client.post(self.get_full_url(url), data)

    def get_export_support(self, invoice_id):
        path_url = 'export-supports/{id}'
        url = path_url.format(id=invoice_id)
        return self.api_client.get(self.get_full_url(url))

    def get_export_support_by_company(self, invoice_id, company_id):
        path_url = 'export-supports/{id}/idCompany/{idCompany}'
        url = path_url.format(id=invoice_id, idCompany=company_id)
        return self.api_client.get(self.get_full_url(url))

    def generate_debit_note(self, data={}):
        url = 'debit-notes'
        return self.api_client.post(self.get_full_url(url), data)

    def get_debit_note(self, debit_note_id):
        path_url = 'debit-notes/{id}'
        url = path_url.format(id=debit_note_id)
        return self.api_client.get(self.get_full_url(url))

    def get_debit_note_by_company(self, debit_note_id, company_id, wait=2):
        path_url = 'debit-notes/{id}/idCompany/{idCompany}'
        url = path_url.format(id=debit_note_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, debit_note_id, company_id,
                                     'get_debit_note_by_company', wait)
        return response

    def generate_credit_note(self, data={}):
        url = 'credit-notes'
        return self.api_client.post(self.get_full_url(url), data)

    def get_credit_note(self, credit_note_id):
        path_url = 'credit-notes/{id}'
        url = path_url.format(id=credit_note_id)
        return self.api_client.get(self.get_full_url(url))

    def get_credit_note_by_company(self, credit_note_id, company_id, wait=2):
        path_url = 'credit-notes/{id}/idCompany/{idCompany}'
        url = path_url.format(id=credit_note_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, credit_note_id, company_id,
                                     'get_credit_note_by_company', wait)
        return response

    def generate_purchase(self, data={}):
        url = 'purchases'
        return self.api_client.post(self.get_full_url(url), data)

    def get_purchase(self, purchase_id):
        path_url = 'purchases/{id}'
        url = path_url.format(id=purchase_id)
        return self.api_client.get(self.get_full_url(url))

    def get_purchase_by_company(self, purchase_id, company_id):
        path_url = 'purchases/{id}/idCompany/{idCompany}'
        url = path_url.format(id=purchase_id, idCompany=company_id)
        return self.api_client.get(self.get_full_url(url))

    def generate_minor_expense(self, data={}):
        url = 'minor-expenses'
        return self.api_client.post(self.get_full_url(url), data)

    def get_minor_expense(self, minor_expense_id):
        path_url = 'minor-expenses/{id}'
        url = path_url.format(id=minor_expense_id)
        return self.api_client.get(self.get_full_url(url))

    def get_minor_expense_by_company(self, minor_expense_id, company_id, wait=2):
        path_url = 'minor-expenses/{id}/idCompany/{idCompany}'
        url = path_url.format(id=minor_expense_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, minor_expense_id, company_id,
                                     'get_minor_expense_by_company', wait)
        return response

    def generate_payment_abroad(self, data={}):
        url = 'payment-abroad-supports'
        return self.api_client.post(self.get_full_url(url), data)

    def get_payment_abroad(self, payment_abroad_id):
        path_url = 'payment-abroad-supports/{id}'
        url = path_url.format(id=payment_abroad_id)
        return self.api_client.get(self.get_full_url(url))

    def get_payment_abroad_by_company(self, payment_abroad_id, company_id):
        path_url = 'payment-abroad-supports/{id}/idCompany/{idCompany}'
        url = path_url.format(id=payment_abroad_id, idCompany=company_id)
        return self.api_client.get(self.get_full_url(url))

    def generate_cancellation(self, data={}):
        url = 'cancellations'
        return self.api_client.post(self.get_full_url(url), data)

    def get_cancellation(self, cancellation_id):
        path_url = 'cancellations/{id}'
        url = path_url.format(id=cancellation_id)
        return self.api_client.get(self.get_full_url(url))

    def get_cancellation_by_company(self, cancellation_id, company_id, wait=2):
        path_url = 'cancellations/{id}/idCompany/{idCompany}'
        url = path_url.format(id=cancellation_id, idCompany=company_id)
        response = self.api_client.get(self.get_full_url(url))
        response = self.get_finished(response, cancellation_id, company_id,
                                     'get_cancellation_by_company', wait)
        return response

    def get_finished(self, response, object_id, company_id, caller, wait):
        caller = getattr(self, caller)
        if response.status_code == 200:
            status = response.json().get('status')
            if status == 'FINISHED' or status == 'FAILED':
                return response
            else:
                print(f'Reintentando despues de {wait} segundos')
                time.sleep(wait)
                wait += wait
                return caller(object_id, company_id, wait)
        return response
